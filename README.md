
## Assignment_APP

To Create a Kubernetes cluster and setup Jenkins on the cluster.Then write a Jenkins pipeline for CI/CD that deploys an app on the same cluster.

## Acknowledgements

I would like to express my special thanks of gratitude to Cliff.ai and Greendeck for giving me this project and assignment i learned a lot of things with this assignment.


## Appendix

For this assignment jenkins and kubernetes is new for me but this assignment gives me a opportunity to learn new technology and devops tools.


## Authors

- [@Harsh Sharma](https://gitlab.com/harsh.sharma4)


## Deployment
metadata:
  annotations:
    deployment.kubernetes.io/revision: "1"
    dev.okteto.com/last-updated: 2022-06-13T19:41:26
  creationTimestamp: "2022-06-13T19:41:26Z"
  generation: 1
  labels:
    app: my-jenkins
  name: my-jenkins
  namespace: utkarsh143
  resourceVersion: "914768487"
  uid: 325efb01-a0d5-433c-8f0e-2da20b148d3f
spec:
  progressDeadlineSeconds: 600
  replicas: 1
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      app: my-jenkins
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: my-jenkins
    spec:
      containers:
      - image: jenkins/jenkins:lts
        imagePullPolicy: IfNotPresent
        name: jenkins
        resources: {}
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
      dnsPolicy: ClusterFirst
      restartPolicy: Always
      schedulerName: default-scheduler
      securityContext: {}
      terminationGracePeriodSeconds: 30
status:
  availableReplicas: 1
  conditions:
  - lastTransitionTime: "2022-06-13T19:43:40Z"
    lastUpdateTime: "2022-06-13T19:43:40Z"
    message: Deployment has minimum availability.
    reason: MinimumReplicasAvailable
    status: "True"
    type: Available
  - lastTransitionTime: "2022-06-13T19:41:26Z"
    lastUpdateTime: "2022-06-13T19:43:40Z"
    message: ReplicaSet "my-jenkins-5cd895bf47" has successfully progressed.
    reason: NewReplicaSetAvailable
    status: "True"
    type: Progressing
  observedGeneration: 1
  readyReplicas: 1
  replicas: 1
  updatedReplicas: 1


## Kubeconfig

Kube.config

apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSURLekNDQWhPZ0F3SUJBZ0lSQUtzS1QzV1NTOWR0Sk5laUY3SEFjbm93RFFZSktvWklodmNOQVFFTEJRQXcKTHpFdE1Dc0dBMVVFQXhNa1l6QTBNRGt3TWpJdE1XSTNZaTAwWlRFMkxXRmhNbVV0TnpBMk1EVTJaV0V5T1dObQpNQjRYRFRJeE1ETXhNakV3TWpVek0xb1hEVEkyTURNeE1URXhNalV6TTFvd0x6RXRNQ3NHQTFVRUF4TWtZekEwCk1Ea3dNakl0TVdJM1lpMDBaVEUyTFdGaE1tVXROekEyTURVMlpXRXlPV05tTUlJQklqQU5CZ2txaGtpRzl3MEIKQVFFRkFBT0NBUThBTUlJQkNnS0NBUUVBanBMOER1ODJUc2YwR3FOaTQ0VWR3dXNiVGNTaDFPellVWHhnemtUOQprR3FKZm80RnQ3N09SMGNCek40b1BCSzMrM1doWjNFRFg4b1V6bVJiaTdaeGNPR01sSlF5OFA3MDVIMUxnNWtKCkN0R3JpWnpPai9UNER1bEZMdW9FWGR0alJFQzdDWFhNalV6cmVDanRRLzZZQ1R0QTk4Y01pZS82UUtUaG5Vc2IKZ1FvYTBFZFlJRzg3OTUzQWY3cEVhV3Fabys0a0J1eHgxMyt6MjFIVVJRa0V3V0xxTHhXWCtyTzE3TWJsYUNVegozNkJtY2tPaHhnVHlCdXNvRUMwbS80VTg5T2dlREVPYzhiUi92NWkvTXBOWEpsd2JLMFRmdWJZNVEzRlIraW9mCnl4Q1I5Z1U4K29ma3FhbnNFckZWQzFUcEcvRkQ4b1czcHF1dFRubXZnYmZqaHdJREFRQUJvMEl3UURBT0JnTlYKSFE4QkFmOEVCQU1DQWdRd0R3WURWUjBUQVFIL0JBVXdBd0VCL3pBZEJnTlZIUTRFRmdRVTFKK1RrMDQrd2dHSQphWG0xZE1oOFVIK3BBTG93RFFZSktvWklodmNOQVFFTEJRQURnZ0VCQUNIc29VVnlUUXhFbUxTOE9RNmgzVWlRCkt4MzBLTEE0QXQrS2ZlMTZsUUM0TkxoYXhEQ09qelFaOXJoT0labVF4VHhBYXlWUlNEd1ZCOEtnV3BaREJxL3MKemdkNW4wb1BNbitJMnExNm5NUnNXYm1mQkhYVE9oemNpS3BsZTFPdkk2aVUyazRvLy9wOVhDSHltUFhzSHA2QQp1R3hza0Nsdm9uQ0ppaDY3T2ltcGMwcmMvaU9QRjQ3Q0dyYytOZXFFZGNxWk5rYnZpNkV1ajhLaC8xWEk1S2pYCi9FUWFMV2RVUGdiRVBxV1ZzL1grT200ZG5IamZib3lrb2VJMW04UktUOTYxYmE5anBLNCtMV0oxeVNzRmZlMGMKZFdmbmxudUNHZHVJeHZsRy9tK2RQaGROMnQwbURIOGVWZjN3N0srSmE4YlpVcGhPWDlQU0ZoNFZwWEdZaURvPQotLS0tLUVORCBDRVJUSUZJQ0FURS0tLS0tCg==
    server: https://146.148.56.200:443
  name: cloud_okteto_com
contexts:
- context:
    cluster: cloud_okteto_com
    namespace: utkarsh143
    user: 703113b4-acf0-40d6-9d37-243fd23cc2ff
  name: cloud_okteto_com
current-context: cloud_okteto_com
kind: Config
preferences: {}
users:
- name: 703113b4-acf0-40d6-9d37-243fd23cc2ff
  user:
    token: eyJhbGciOiJSUzI1NiIsImtpZCI6IlBJWWEyU3FyUm5LcGlhNTRDZGU3U2tDRkpRSDVicThCcC1qbFFnN2JtNjgifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJva3RldG8iLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlY3JldC5uYW1lIjoiNzAzMTEzYjQtYWNmMC00MGQ2LTlkMzctMjQzZmQyM2NjMmZmLXRva2VuLW1qZ2cyIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6IjcwMzExM2I0LWFjZjAtNDBkNi05ZDM3LTI0M2ZkMjNjYzJmZiIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50LnVpZCI6IjMwMjI1ZWRkLTZmNTgtNGY1OS1iYjZlLWU3Mjc1Zjg1ZTM3OCIsInN1YiI6InN5c3RlbTpzZXJ2aWNlYWNjb3VudDpva3RldG86NzAzMTEzYjQtYWNmMC00MGQ2LTlkMzctMjQzZmQyM2NjMmZmIn0.CAiCi-tQ05ir3JzzikJy_7yeYVvD7uhxRSpxOeF8WaD7hBtYrWft1fuC9eQJJP3rfmzEsstZixIOw02zFkCHahhg_bAjRJWXOgzx1MfgWUqhFDr3FeOBoLbpkC937uz5zscfBzA_m-x5DJMTil-89WECpYEdVi8o4GSk4AKr31xAkv_CT8toimJraJzEr_6VpiklqjLGtlkRJUAo9xVxyKeYD_PV444Eh73QHF4ZUrsUhYVCTIr60jx_HktX3sA2BKnP_lhW3XX3-92jLU518k6wIWsfdUqD45p4sdpE9u0kbxo7VUO7uXPZ9MCmTTrLRNk23IqwPzMPGLJQXH4jQA



## Demo

Its in a zip file.


## Deployment

#!/bin/bash
echo "***-Starting CI CD Pipeline Tasks-**"
#-BUILD
echo ""
echo "....Build Phase Started :: Compiling Source Code :: ....."
cd java_web_code
mvn install

#-BUILD (TEST)
echo ""
echo "......Test Pase Started :: Testing via Automated Scripts :: ......"
cd ../integration-testing/
mvn clean verify -P integration-test

## Documentation

We need to create a kubernetes cluster first and then setup a jenkins on that cluster using CLI.Then setup a jenkins CI/CD pipeline to create a job 1,2,3 for build test.
 and deploy the application.Then after we have to fork the gitlab repository to our local account/id.Then we need to pulled the source code management which i have forked
  into the local gitlab account repository and jenkins in this stage.The Docker conatainerisation image will be created in the second stage and pushed to a private repository
   and in last stage the pushed image will be deployed as a deployment with high-availability in k8s cluster.
 








## FAQ

1-On Okteto, kubernetes ingress with automatic SSL not working.I tried the steps from documentations and the ingress service was not created-https://www.okteto.com/docs/cloud/ssl/ 

Answer 1-Instead of Okteto I used jenkins to make CI/CD pipeline to create a different job1,2,3 for Build,test and deploy in local host 8080.




## Feedback

If you have any feedback, please reach out to us at pk526272@gmail.com


## 🚀 About Me
To seek a challenging career where there is growth in knowledge, experience and excel in the field through hard work, 
research, skills and perseverance.Believe in hard work and attaining big goals in short span of time.Seeking a position
 where I can utilize my knowledge Abilities and Personal Skills while being Resourceful And flexible that’s offers professional
  growth along with the organization and to have growth oriented and challenging care and experience through continuous learning and team work.


## 🛠 Skills
AWS,Linux,Shell Scripting,GIT,Python.



# Hi, I'm Utkarsh143! 👋


## 🔗 Links
[![linkedin](linkedin.com/in/utkarsh-gautam-716085a3)
[![twitter](https://twitter.com/Utkarshgautam14)


## Other Common Github Profile Sections
👩‍💻 I'm currently working on Kodekloud Certifications

🧠 I'm currently learning Kubernetes

👯‍♀️ I'm looking for full time Devops role jobs.

🤔 I'm looking for help with technical skills and knowledge.


📫 How to reach me...+91-9945894969
pk526272@gmail.com


## Installation

sudo apt install git
 sudo apt install -y curl wget apt-transport-https
 sudo apt install minikube
 sudo apt install kubectl
 sudo apt install jenkins.io 
 sudo apt install docker.io 
 

## Lessons Learned

What did you learn while building this project? What challenges did you face and how did you overcome them?

The Biggest challenges is that i dont know much about the docker,kubernetes and Jenkins but for the completion of assignment i overcome with this, taking help of youtube videos,stackflow and google reference
 with this i learned a lot of things about the kubernetes, jenkins and docker and the build,test and deployment process.
## Run Locally

Clone the project

```bash
  git clone git@gitlab.com:cliff-public/assignment_app.git
```
Pull the project
```bash
git pull git@gitlab.com:cliff-public/assignment_app.git

Go to the project url

```bash
  https://gitlab.com/cliff-public/assignment_app/-/tree/main
```

Install dependencies

```bash
sudo apt install git
 sudo apt install -y curl wget apt-transport-https
 sudo apt install minikube
 sudo apt install kubectl
 sudo apt install jenkins.io 
 sudo apt install docker.io 


